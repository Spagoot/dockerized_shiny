library(shiny)
library(ggplot2)
library(data.table)
library(shinydashboard)

PSL_Data_BG2_12V <- read.csv('PSL_Data_BG2_12V.csv', row.names=1, header=TRUE, sep=";")

source('server.R', local = TRUE)
source('ui.R', local = TRUE)

shinyApp(ui = ui, server = server)
