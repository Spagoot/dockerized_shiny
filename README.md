# Dockerized Shiny


This Repo includes a simple Shiny App which has been prepared to be deployed in a docker container.

---

### Why should you put shiny Apps in a Docker Container?

* Scale out for balancing load peaks
* Easy deployment in Azure
* Using Azures builtin Authentication/Authorization Features

---

### Thanks to Bjoern Hartmann
The Docker Stuff was done by the help of his Blog:
https://www.bjoern-hartmann.de/post/learn-how-to-dockerize-a-shinyapp-in-7-steps/